#SvnAdmin
### 纯JAVA的SVN管理工具
已增加H2数据库支持

安装：
打War包放到Servlet容器即可
```bash
gradlew war
```

注意

- 原版本要求JDK5，我使用JDK7编译，没测试JDK5
- 默认使用H2数据库，H2数据库文件放在~/svnadmin
- 数据库配置文件在 src/main/webapp/WEB-INF/jdbc.properties
- 升级SVNKIT到1.8